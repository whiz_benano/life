const header = document.querySelector('header')
const menuWhite = document.querySelector('.menu-white')
const navPos = document.querySelectorAll('.posNav')
const loader = document.querySelector('.loader')
const section = document.querySelector('body')

// progressBar start

const animateLoader = () => {
    let scrollDistance = -section.getBoundingClientRect().top
    let progressWidth = (scrollDistance / (section.getBoundingClientRect().height - document.documentElement.clientHeight)) * 100
    progressWidth = Math.floor(progressWidth)
    loader.style.width = `${progressWidth}%`
}
// progressBar end

window.addEventListener('scroll', () => {
    header.classList.toggle('sticky', scrollY > 0)
    animateLoader()
})

menuWhite.addEventListener('click', () => {
    header.classList.toggle('open')
    menuWhite.classList.toggle('close')
})

navPos.forEach(jump => {
    jump.addEventListener('click', () => {
        header.classList.toggle('open')
        if(menuWhite.classList.contains('close'))
            menuWhite.classList.remove('close')
    })
})
